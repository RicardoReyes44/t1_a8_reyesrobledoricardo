'''
Created on 9 mar. 2021

@author: RSSpe
'''

from tkinter import *
import math
from tkinter import messagebox as msg


class Calculadora:
    
    def __init__(self):
        self.__operacion = ""
        self.__raiz = Tk()
        self.__raiz.title("Calculadora")
        
        self.__raiz.resizable(0,0)
        
        self.__caja = StringVar()
        
        self.__frame = Frame(self.__raiz)
        self.__frame.pack()
        
        self.__frame.config(bg="#F0F0F0", cursor="hand2")
        
        # Caja de texto
        self.__txtResultado = Entry(self.__frame, font=("bold", 22), justify="right", borderwidth=0, state=DISABLED, textvariable=self.__caja).grid(row=0, column=0, columnspan=4)
        self.__caja.set("0")

        # Botones
        self.__btnPorcentaje = Button(self.__frame, text="%", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.realizarOperacion("%")).grid(row=1, column=0)
        self.__btnRaiz = Button(self.__frame, text="√", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.realizarOperacion("√")).grid(row=1, column=1)
        self.__btnCuadrado = Button(self.__frame, text="x²", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.realizarOperacion("x²")).grid(row=1, column=2)
        self.__btnUnoSobreX = Button(self.__frame, text="1/x", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.realizarOperacion("1/x")).grid(row=1, column=3)
        
        self.__btnCE = Button(self.__frame, text="CE", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=self.eliminarCE).grid(row=2, column=0)
        self.__btnC = Button(self.__frame, text="C", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=self.eliminarC).grid(row=2, column=1)
        self.__btnBorrar = Button(self.__frame, text="<=", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=self.borrarCaracter).grid(row=2, column=2)
        self.__btnDiv = Button(self.__frame, text="/", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.realizarOperacion("/")).grid(row=2, column=3)
        
        self.__btnSiete = Button(self.__frame, text="7", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.ingresarNum(7)).grid(row=3, column=0)
        self.__btnOcho = Button(self.__frame, text="8", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.ingresarNum(8)).grid(row=3, column=1)
        self.__btnNueve = Button(self.__frame, text="9", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.ingresarNum(9)).grid(row=3, column=2)
        self.__btnPor= Button(self.__frame, text="x", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.realizarOperacion("x")).grid(row=3, column=3)
        
        self.__btnCuatro = Button(self.__frame, text="4", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.ingresarNum(4)).grid(row=4, column=0)
        self.__btnCinco = Button(self.__frame, text="5", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.ingresarNum(5)).grid(row=4, column=1)
        self.__btnSeis = Button(self.__frame, text="6", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.ingresarNum(6)).grid(row=4, column=2)
        self.__btnMenos = Button(self.__frame, text="-", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.realizarOperacion("-")).grid(row=4, column=3)

        self.__btnUno = Button(self.__frame, text="1", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.ingresarNum(1)).grid(row=5, column=0)
        self.__btnDos = Button(self.__frame, text="2", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.ingresarNum(2)).grid(row=5, column=1)
        self.__btnTres = Button(self.__frame, text="3", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.ingresarNum(3)).grid(row=5, column=2)
        self.__btnMas = Button(self.__frame, text="+", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.realizarOperacion("+")).grid(row=5, column=3)
        
        self.__btnMasMenos = Button(self.__frame, text="+-", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=self.cambiarSigno).grid(row=6, column=0)
        self.__btnCero = Button(self.__frame, text="0", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.ingresarNum(0)).grid(row=6, column=1)
        self.__btnPunto = Button(self.__frame, text=".", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=self.punto).grid(row=6, column=2)
        self.__btnIgual = Button(self.__frame, text="=", font=("bold", 15), borderwidth=0, width=7, height=2, background="#C0C0C0", command=lambda:self.realizarOperacion("=")).grid(row=6, column=3)


    def porcentaje(self):
        self.__operacion = f"{self.__caja.get()}%"
        self.__caja.set(self.__operacion)


    def suma(self):
        self.__operacion = f"{self.__caja.get()}+"
        self.__caja.set(self.__operacion)

    
    def mult(self):
        self.__operacion = f"{self.__caja.get()}*"
        self.__caja.set(self.__operacion)
    
    
    def resta(self):
        self.__operacion = f"{self.__caja.get()}-"
        self.__caja.set(self.__operacion)
    
    
    def div(self):
        self.__operacion = f"{self.__caja.get()}/"
        self.__caja.set(self.__operacion)
    
    
    def realizarOperacion(self, opcion):
        
        try:
            
            if opcion=="√":
                self.raiz()
            elif opcion=="x²":
                self.cuadrado()
            elif opcion=="1/x":
                self.unoSobreX()
            elif opcion=="+":
                self.suma()
            elif opcion=="-":
                self.resta()
            elif opcion=="x":
                self.mult()
            elif opcion=="/":
                self.div()
            elif opcion=="=":
                self.igual()
            elif opcion=="%":
                self.porcentaje()
        
        except NameError as e:
            self.indicarError()
            self.eliminarC()

        except SyntaxError as e:
            self.indicarError()
            self.eliminarC()
        
        except ZeroDivisionError as e:
            self.indicarError()
            self.eliminarC()

    
    def indicarError(self):
        msg.showerror("Error", "No puedes realizar esa operacion, por favor prueba de nuevo")
 
 
    def cuadrado(self): 
        if not self.__caja.get()=="0":
            self.__operacion=eval(f"pow({self.__caja.get()}, 2)")
            self.__caja.set(self.__operacion)


    def punto(self):
        texto=self.__caja.get()
        candado = False
        
        for i in texto:
            if i==".":
                candado = True
        
        if not candado:
            self.__operacion=f"{self.__caja.get()}."
            self.__caja.set(self.__operacion)


    def eliminarC(self):
        self.__caja.set("0")
        self.__operacion = ""


    def cambiarSigno(self):
        texto=self.__caja.get()
        
        if not texto=="0":
            if texto[0]=="-":
                texto=texto.replace(texto[0], "")
            else:
                texto="-"+texto
        
        self.__operacion=texto
        self.__caja.set(texto)


    def raiz(self):
        if not self.__caja.get()=="0":
            try:
                self.__operacion = math.sqrt(eval(f"{self.__operacion}"))
                self.__caja.set(self.__operacion)
            except ValueError as e:
                self.indicarError()
                self.eliminarC()


    def igual(self):
        self.__operacion = eval(self.__operacion)
        self.__caja.set(self.__operacion)


    def eliminarCE(self):
        self.__caja.set("0")


    def borrarCaracter(self):
        texto = self.__caja.get()
        longitud = len(texto)
        
        if not longitud==1:
            self.__caja.set(texto[:longitud-1])
        else:
            self.__caja.set("0")

       
    def unoSobreX(self): 
        self.__operacion=eval(f"1/({self.__caja.get()})")
        self.__caja.set(self.__operacion)


    def ingresarNum(self, num):
        texto = self.__caja.get()

        if len(texto)==1 and texto[0]=="0":
            self.__operacion=f"{self.__caja.get()[1:]}{num}"
        else:
            self.__operacion=f"{self.__caja.get()}{num}"
            
        self.__caja.set(self.__operacion)


    def construir(self):
        self.__raiz.mainloop()
